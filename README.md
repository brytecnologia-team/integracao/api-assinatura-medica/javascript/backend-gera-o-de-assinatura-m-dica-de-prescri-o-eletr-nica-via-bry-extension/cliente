### Inclusão de Metadados em Prescrição Eletrônica com Assinatura PDF via BRy Extension

Resumo das funcionalidades:
1. Simula a conexão com o servidor local;
2. Permite utilizar a extensão para a geração de assinatura PDF no Windows, Linux e MacOS.

Para que a aplicação cliente funcione corretamente, é necessário configurar a extensão para permitir URLs locais. Deve-se realizar os seguintes passos:
1. Abra o arquivo assinatura.html;
2. A página solicitará a instalação da extensão, siga os passos de instalação;
3. Perceba que, quando voltar para a página da assinatura, ainda não será detectada a extensão. É necessário acessar as extensões (chrome://extensions), localize a extensão e marque a opção "Permitir acesso aos URLs do arquivo";
4. Execute o servidor.

O botão "Inicializar assinatura (Server-Framework) executa a função "inicializar" do arquivo "script-customizavel.js". Essa função acessa o servidor local (servidor) e envia os dados do certificado. A informação necessária do lado cliente para inicializar a assinatura no servidor é o conjunto de bytes do certificado. O retorno da chamada "inicializar" é o "retorno do processo de inicialização da assinatura no Framework" já transformado para ser utilizado pela extensão.
A função “BryApiModule.sign” utiliza os dados preparados (input) pela função anterior e assina as informações com o certificado selecionado pelo usuário, informando o “idSelectedCertificate”.
O próximo passo é a finalização da assinatura, função BryApiModule.finalizar, o exemplo realiza um post para o servidor local (servidor), passando esses dados de retorno da extensão. No servidor local, esses dados são processados e enviados para o BRy HUB.


### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a BRy Extension no seu browser, assim como ter um certificado importado.

### Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  
